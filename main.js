document.addEventListener('DOMContentLoaded', function(event) {

	var url = "https://raw.githubusercontent.com/FreeCodeCamp/ProjectReferenceData/master/cyclist-data.json";

	fetch(url)
		.then(function(response) {
			return response.json();
		})
		.then(function(data) {

			var margin = {top: 10, right: 100, bottom: 70, left: 70},
				fullWidth = 900,
				fullHeight = 760;

			// used for ranges of the scales
			var width = fullWidth - margin.right - margin.left;
			var height = fullHeight - margin.top - margin.bottom;

			var minTime = 5000;
			var maxTime = 0;
			var runTime = data.map(obj => {
				var orgTime = obj.Time.split(":");
				var minutes = orgTime[0]*60;
				var seconds = orgTime[1]*1;
				var time = minutes + seconds;
				if(time > maxTime) {
					maxTime = time;				
				}
				if(minTime > time) {
					minTime = time;
				}
				return maxTime;
			})
			
			// Scales
			var x = d3.scaleLinear()
				.domain([maxTime, minTime])
				.range([0, width]);

			var y = d3.scaleLinear()
					.domain([0, 36])
					.range([0, height]);

			// Axes
			var xAxis = d3.axisBottom()
				.scale(x)
				.ticks(10);
			var yAxis = d3.axisLeft()
				.scale(y)
				.ticks(6);

			// define tooltip
			var tooltip = d3.select("body")
				.append("div")
				.attr("class", "tooltip")
				;

			// set up chart
			var chart = d3.select(".chart")
				.attr("width", fullWidth)
				.attr("height", fullHeight)
				.append("g")
					.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

			// adding axes to chart
			chart.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + height + ")")
				.call(xAxis);
			
				chart.append("g")
					.attr("class", "y axis")
					.attr("style", "padding")
					.call(yAxis);

			// Chart legend
			var legend = d3.select("body")
				.append("div")
				.attr("class","legend")
				.attr("transform","translate(50,30)")
				.style("font-size","12px")

			// text label for axes
			chart.append("text")
				.attr("transform", "translate(" + (width/2) + "," + (height + margin.top + 30) +")")
				.text("Time in Seconds");

			chart.append("text")
				.attr("transform", "rotate(-90)")
				.attr("y", 0 - margin.left)
				.attr("x", 0 - height/2)
				.attr("dy", "1em")
				.style("text-anchor", "middle")
				.text("Rank");	

				
			// add data to chart
			var circles = chart.selectAll(".circle")
				.data(data)
				.enter()
					.append("circle")
			
			var doping = function(d) {
				var tooltipHTML = "";
				if (d.doping !== "") {
							tooltipHTML += "<br/>" + d.Doping;
						} else {
							tooltipHTML += "<br/> No doping allegations";
						}
						return tooltipHTML;
					}

			var circleAttrs = circles
				// position on x,y axes
				.attr("cx", function(d, i) { return x(runTime[i]) })
				.attr("cy", function(d) { return y(d.Place)})
				.attr("r", function(d) { return 7 })	
				// .style("fill", "#9100ff")	
				.style("fill", function(d) {
					var returnColor;
					if(d.Doping == "") {returnColor = "#000000";}
					else returnColor="#9100ff";

					return returnColor;
				})
				.on("mouseover", function(d) {
					tooltip.transition()
						.duration(200)
						.style("opacity", 1);

					tooltip.html(`<strong> ${d.Name}: </strong> ${d.Nationality} <br />
						Year: ${d.Year}
						Time: ${d.Time}<br />
						${doping(d)}
					`)
						.style("left", (d3.event.pageX) - 50 + "px")
						.style("top", (d3.event.pageY) - 60 + "px");
					})
				.on("mouseout", function(d) {
					tooltip.transition()
						.duration(500)
						.style("opacity", 0);
	
					});
		});
// end document.addEventListener
});
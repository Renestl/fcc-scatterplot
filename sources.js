document.addEventListener('DOMContentLoaded', function(event) {

	var url = "https://raw.githubusercontent.com/FreeCodeCamp/ProjectReferenceData/master/cyclist-data.json";

	fetch(url)
		.then(function(response) {
			return response.json();
		})
		.then(function(data) {

			// List sources to source page
			data.map( obj => {
				var sources = document.getElementById("sources");
				sources.innerHTML = data.map(obj => {
					return `<li> 
										<strong>${obj.Name.toString()}:</strong> <a href="${obj.URL.toString()}">${obj.URL.toString()}</a>	
									</li>`
				}).join('');
			});
					
		});
// end document.addEventListener
});
# FCC Scatterplot Graph

[Live Demo]()

## Description
This project utilizes d3.js to display a scatterplot that visualizes doping in professional bicyle racing.

When the user places the cursor over one of the plot points, a tooltip will be displayed and provide additional information.

## Uses
* HTML
* CSS
* JavaScript
* D3.js

## About
This project was written as a requirement in the [Data Visualization Projects](https://www.freecodecamp.org/challenges/visualize-data-with-a-scatterplot-graph)
 for [FreeCodeCamp](https://www.freecodecamp.org)


## License
[MIT](https://tldrlegal.com/license/mit-license) &copy; 2018 [Jennifer Currie](https://github.com/Renestl)